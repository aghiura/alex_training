import styles from './Medicine.module.css';
import {fetchMedication,addMedication} from '../../slices/medicationSlice'
import { useSelector, useDispatch } from 'react-redux'

import { Autocomplete } from '@material-ui/lab';
import {TextField, Modal} from '@material-ui/core'
import { useEffect, useState} from 'react';
import MedicationForm from '../../Components/MedicationForm/MedicationFrom'
import { useHistory } from 'react-router-dom';
require("dotenv").config();

function Medicine() {
  const history=useHistory()
  const medication = useSelector((state) => state.medication.medication)

  const dispatch = useDispatch()
  const [open,setOpen]=useState(false)
  const [value,setValue]=useState({
    "name":"",
    "producer":"",
    "usage":"",
    "phone":""
})

// const callBackendAPI = async () => {
//   const response = await fetch('/message/test');
//   const body = await response.json();

//   if (response.status !== 200) {
//     throw Error(body.message) 
//   }
//   return body;
// };


  useEffect(()=>{
    dispatch(fetchMedication()) 
  },[dispatch])


  const clickHandler=async()=>{
    setOpen(true)
    const token=window.localStorage.getItem('token')
    console.log(token)
  }

  const goToListHandler=()=>{
    history.push('/medicine-list')
  }

  const goToStatistics=()=>{
    history.push('/statistics')
  }
  const saveHandler=(name,producer,usage,phone)=>{
    const medicine={
      "name":name,
      "producer":producer,
      "usage":usage,
      "phone":phone
        }

    dispatch(addMedication(medicine))
    setOpen(false)
  }
  const onValueChange=(event,values)=>{
    setValue(values);
    console.log(values);
  }
  return (
    <div className={styles.AppContainer}>
    <div className={styles.MedicationContainer}>
       <Modal
        open={open}
      >
        <div>
        <MedicationForm saveHandler={saveHandler}/>
        </div>
      </Modal>
     <Autocomplete
          id="name-box"
          value={value}
          getOptionSelected={(option, value) => true}
          onChange={onValueChange}
          options={medication}
          getOptionLabel={(option) => option.name}
          style={{ width: '20%',background:'white' }}
          renderInput={(params) => <TextField color="primary" {...params} label="Name"  variant="filled" />}
        />
        <Autocomplete
          id="producer-box"
          getOptionSelected={(option, value) => true}
          onChange={onValueChange}
          value={value}
          options={medication}
          getOptionLabel={(option) => option.producer}
          style={{ width: '20%',background:'white' }}
          renderInput={(params) => <TextField {...params} label="Producer"  variant="filled"/>}
        />
        <Autocomplete
          id="usage-box"
          onChange={onValueChange}
          getOptionSelected={(option, value) => true}
          value={value}
          options={medication}
          getOptionLabel={(option) => option.usage}
          style={{ width: '20%',background:'white' }}
          renderInput={(params) => <TextField {...params} label="Usage"  variant="filled"/>}
        />
         
         <Autocomplete
          id="expiration-box"
          onChange={onValueChange}
          getOptionSelected={(option, value) => true}
          value={value}
          options={medication}
          getOptionLabel={(option) => option.phone}
          style={{ width: '20%',background:'white' }}
          renderInput={(params) => <TextField {...params} label="Phone number"  variant="filled"/>}
        />
        
    </div>
    <div className={styles.ButtonContainer}>
       <button onClick={clickHandler} className={styles.ButtonStyle}>Add new medication</button>
       <button onClick={goToListHandler}className={styles.ButtonStyle}>Medication list</button>
       <button onClick={goToStatistics} className={styles.ButtonStyle}>Statistics</button>
    </div>
    </div>
  );
}

export default Medicine;
