import React from "react";
import Bar from './Chart'
import schc from "./SCHC.json";
import vcit from "./VCIT.json";
import PieSVG from './PieChart'
import styles from './Statistics.module.css'

 
const schcData = {
    name: "SCHC",
    color: "#d53e4f",
    items: schc.map((d) => ({ ...d, date: new Date(d.date) }))
  };
  const vcitData = vcit.map((d) => ({ ...d, date: new Date(d.date) }))
  
 
function Statistics(){
  return (
    <div className={styles.StatisticsContainer}>
        <Bar
          data={schcData}
          width={800}
          height={200}
          top={20}
          bottom={30}
          left={100}
          right={0}
        />
      <div>
        <span className="label">SVG Elements</span>
        <PieSVG
          data={vcitData}
          width={200}
          height={200}
          innerRadius={60}
          outerRadius={100}
        />
      </div>
    </div>
  );
}

export default Statistics
  
