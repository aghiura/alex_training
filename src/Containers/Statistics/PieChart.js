import React from "react";
import * as d3 from "d3";

const Arc = ({ data, index, text,createArc, colors, format }) => {
  console.log(data)
  let rotation =  data.endAngle < Math.PI ? 
    (data.startAngle/2 + data.endAngle/2) * 180/Math.PI : 
    (data.startAngle/2  + data.endAngle/2 + Math.PI) * 180/Math.PI
  return(<g key={index} className="arc">
    <path className="arc" d={createArc(data)} fill={colors(index)} />
    <text
      transform={`translate(${createArc.centroid(data)}),rotate(-90),rotate(${rotation})`}
      textAnchor="middle"
      alignmentBaseline="middle"
      fill="white"
      fontSize="8"
    >
      {data.data.problem}
    </text>
  </g>)
};

const Pie = props => {    
  const createPie = d3
    .pie()
    .value(d => d.cases)
    .sort(null);
  const createArc = d3
    .arc()
    .innerRadius(props.innerRadius)
    .outerRadius(props.outerRadius);
  const colors = d3.scaleOrdinal(d3.schemeCategory10);
  const format = d3.format(".2f");
  const data = createPie(props.data);

  return (
    <svg width={props.width} height={props.height}>
      <g transform={`translate(${props.outerRadius} ${props.outerRadius})`}>
        {data.map((d, i) => (
          <Arc
            key={i}
            data={d}
            index={i}
            text={d}
            createArc={createArc}
            colors={colors}
            format={format}
          />
        ))}
      </g>
    </svg>
  );
};

export default Pie;
