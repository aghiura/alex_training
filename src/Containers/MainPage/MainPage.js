import { Card, CardHeader, Grid, IconButton, Modal } from '@material-ui/core'
import MedicationForm from '../../Components/MedicationForm/MedicationFrom'
import { useTranslation } from "react-i18next";
import { Add } from '@material-ui/icons'
import { DataGrid } from '@material-ui/data-grid'
import PieSVG from '../Statistics/PieChart'
import Bar from '../Statistics/Chart'
import vcit from "./VCIT.json";
import schc from "./SCHC.json";
import { withTranslation } from 'react-i18next';
import styles from "./MainPage.module.css"
import { useSelector, useDispatch } from 'react-redux';
import { useEffect, useState } from 'react'
import { fetchMedication } from '../../slices/medicationSlice'


function MainPage() {
  const { t } = useTranslation();
  const [showModal, setShowModal] = useState(false)
  const [list, setList] = useState()
  const schcData = {
    name: "SCHC",
    color: "#d53e4f",
    items: schc.map((d) => ({ ...d, date: new Date(d.date) }))
  };

  const dispatch = useDispatch()
  const medication = useSelector((state) => state.medication.medication)

  useEffect(() => {
    dispatch(fetchMedication())
  }, [dispatch])

  useEffect(() => {
    console.log('list', medication.length)
    if (medication.length > 1)
      setList(medication)
  }, [medication])

  const columns = [
    {
      field: 'name',
      headerName: t('name'),
      width: 150,
      editable: true,
    },
    {
      field: 'company',
      headerName: t('company'),
      width: 150,
      editable: true,
    },
    {
      field: 'producer',
      headerName: t('producer'),
      width: 160,
      editable: true,
    },
    {
      field: 'phone',
      headerName: t('ph_no'),
      sortable: false,
      width: 160,
    },
    {
      field: 'expirationDate',
      headerName: t('exp_date'),
      sortable: false,
      width: 160,
    },
    {
      field: 'usage',
      headerName: t('usage'),
      sortable: false,
      width: 160,
    },
    {
      field: 'country',
      headerName: t('country'),
      sortable: false,
      width: 160,
    },
  ];

  const vcitData = vcit.map((d) => ({ ...d, date: new Date(d.date) }))

  const addClickHandler = () => {
    setShowModal(!showModal)
  }
  console.log(list)
  return (
    <Grid className={styles.MainPageContainer}>
      <Modal open={showModal} >
        <div>
          <MedicationForm saveHandler={addClickHandler} />
        </div>
      </Modal>
      <Grid container justifyContent="space-around" alignItems="flex-end" >
        <Grid container item spacing={3} xs={12} >
          <Grid item sm={6} xs={12} >
            <Card variant='elevation'>
              <Grid container justifyContent="flex-end">
                <Grid item xs={12}>
                  <CardHeader
                    title={t('dis_cases')}
                    subheader="August, 2021" />
                </Grid>
                <Grid item>
                  <PieSVG
                    data={vcitData}
                    width={150}
                    height={150}
                    innerRadius={0}
                    outerRadius={70}
                  />
                </Grid>
              </Grid>
            </Card>
          </Grid>
          <Grid item sm={6} xs={12}>
            <Card>
              <CardHeader
                title={t('no_illness')}
                subheader="August, 2021" />
              <Bar
                data={schcData}
                width={600}
                height={150}
                top={20}
                bottom={30}
                left={50}
                right={0}
              />
            </Card>
          </Grid>
          {/* </div> */}
          <Grid item xs={12}>
            {/* <div className={styles.ListContainer}> */}
            <Card>
              <CardHeader
                title={t('best_medicine')}
                action={
                  <IconButton aria-label="settings" onClick={addClickHandler}>
                    <Add />
                  </IconButton>
                } />
              {list && <DataGrid
                style={{ height: "250px" }}
                rows={list}
                columns={columns}
                pageSize={3}
                checkboxSelection
                disableSelectionOnClick
              />}
            </Card>
            {/* </div> */}
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )

}
export default withTranslation()(MainPage)