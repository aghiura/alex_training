import { TextField } from '@material-ui/core'
import i18next from "i18next";
import { useHistory } from "react-router-dom";
import { useEffect, useState } from "react"
import { useTranslation } from "react-i18next";
import styles from "./Login.module.css"
import { Autocomplete } from '@material-ui/lab';


function Login() {
  const { t } = useTranslation();
  const history = useHistory();
  const [email, setEmail] = useState('')
  const [pass, setPass] = useState('')
  const langList = ['en', 'ro']
  const [lang, setLang] = useState('en')


  useEffect(() => {
    i18next.changeLanguage(lang)
  }, [lang])
  const loginTest = async () => {
    const data = {
      "email": email,
      "password": pass
    }
    const response = await fetch('/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });

    return response.json();

  }

  const changeEmail = (event) => {
    setEmail(event.target.value)
  }
  const changePass = (event) => {
    setPass(event.target.value)
  }
  const loginHandler = () => {
    history.push('/statistics')
    loginTest()
      .then(res => window.localStorage.setItem('token', res.token))
      .catch(err => console.log(err));
  }
  return (
    <div>

      <Autocomplete
        options={langList}
        className={styles.LanguageSelector}
        getOptionSelected={(option, value) => true}
        getOptionLabel={(key) => key}
        value={lang}
        disableClearable
        onChange={(event, newValue) => {
          setLang(newValue);
        }}
        renderInput={(params) => (
          <TextField {...params} label="Language" variant="standard" size='small' />
        )}
      />
      <div className={styles.LoginContainer}>
        <TextField id="email" label={t("email")} onChange={changeEmail} value={email} />
        <TextField id="pass" label={t("pass")} onChange={changePass} value={pass} />
        <button onClick={loginHandler} className={styles.ButtonContainer}>{t("login")}</button>
      </div>
    </div>
  )
}
export default Login;