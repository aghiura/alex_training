import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Medicine from './Containers/Medicine/Medicine'
import Login from './Containers/Login/Login'
import MainPage from './Containers/MainPage/MainPage'

require("dotenv").config();


function App() {
  return(
    <Router>
      <Switch>  
        <Route path='/statistics'>
          <MainPage/>
        </Route>
          <Route path="/medicine">
            <Medicine/>
          </Route>
          <Route path="/">
            <Login />
          </Route>
      </Switch>
    </Router>
  )
  
}

export default App;
