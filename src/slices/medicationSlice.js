import { createSlice } from '@reduxjs/toolkit'

const initialState={
    medication:['']
}

export const medicationSlice=createSlice({
    name:'medication',
    initialState,
    reducers:{
        getMedication:(state,{payload})=>{
            state.medication=payload
        },
        addMedication:(state,action)=>{
            state.medication.push(action.payload)
        }
    }
})
export const {getMedication,addMedication} =medicationSlice.actions

export const  medicationSliceReducer=medicationSlice.reducer


export  const fetchMedication=()=> {
    return async dispatch => {
        const response = await fetch('/medicine',{
                headers: { 
                    'x-access-token': window.localStorage.getItem('token'),
                    'Content-Type' : 'text/plain' 
                }
            
        })
        const body = await response.json();
        dispatch(getMedication(body.medicine))
    }
}