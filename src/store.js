import { configureStore } from '@reduxjs/toolkit'
import { medicationSliceReducer } from './slices/medicationSlice'

export const store = configureStore({
  reducer: {
    medication: medicationSliceReducer
  }
})