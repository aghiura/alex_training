import {
    Card,
    CardContent,
    CardHeader,
    TextField,
    Button,
    CardActions
} from '@material-ui/core'
import { useState } from 'react'
import styles from './MedicationForm.module.css'

function MedicationForm(props) {

    const [name, setName] = useState('')
    const [usage, setUsage] = useState('')
    const [producer, setProducer] = useState('')
    const [expiration, setExpiration] = useState('')

    const changeName = (event) => {
        setName(event.target.value)
    }
    const changeProducer = (event) => {
        setProducer(event.target.value)
    }
    const changeUsage = (event) => {
        setUsage(event.target.value)
    }
    const changeExpiration = (event, field) => {
        setExpiration(event.target.value)

    }
    const clickHandler = () => {
        props.saveHandler(name, producer, usage, expiration)
    }
    return (
        <Card className={styles.FormContainer}>
            <CardHeader title="Add new medicine"></CardHeader>
            <CardContent> 
                <TextField id="name" label="Name" fullWidth onChange={changeName} value={name} />
                <TextField id="producer" label="Producer" fullWidth onChange={changeProducer} value={producer} />
                <TextField id="usage" label="Usage" fullWidth onChange={changeUsage} value={usage} />
                <TextField id="expiration-date" label="Expiration Date" fullWidth onChange={changeExpiration} value={expiration} />
            </CardContent>
            <CardActions>
                <Button size="small" onClick={clickHandler} color="primary">
                    Save
                </Button>
            </CardActions>
        </Card>
    )
}

export default MedicationForm