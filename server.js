const express = require('express'); //Line 1
const jwt = require("jsonwebtoken");
const app = express(); //Line 2
const bodyParser = require('body-parser');
app.use(bodyParser.json())
const port = 5555; //Line 3 //Line 3
const auth = require("./src/middleware/auth");


app.listen(port, () => console.log(`Listening on port ${port}`)); //Line 6

const hello = (req,res)=>{
  res.send({ medicine: 'dsadasdsadas' })
}
app.get("/hello",hello)

const medicine=(req,res)=>{
  const data=require('./data.json')
  res.send({medicine:data})
}

app.get("/medicine",auth,medicine)
// create a GET route
app.get('/message/:value', (req, res) => { //Line 9
  res.send(req.params); //Line 10
}); 


app.post("/login", async (req, res) => {

  try {
    const { email, password } = req;
    const user={"email":email,
                "password":password,
                "token":""}
      const token = jwt.sign(
        { user_id: user._id, email },
        '123456',
        {
          expiresIn: "2h",
        }
      );

      // save user token
      user.token = token;

      // user
      res.status(200).json(user);
  } catch (err) {
    console.log(err);
  }
});